# MetaData

For experimental data to be FAIR when it leaves a research organization, the systems and procedures involved in the experimental life cycle of the facilities concerned must be coordinated to build and maintain the FAIR character of these data. In particular, at each stage of the experimental life cycle, metadata and contextual information should be collected (ideally as automatically as possible, but also manually when necessary), to form as complete and rich a record as possible of the experimental context throughout its cycle from overall project proposal submission to scientific publication so that these data can be FAIR.
The document "Draft Recommendations for FAIR Photon and Neutron Data Management" analyzes the metadata that should be generated and recorded at each stage of the life cycle of an experimental research project, and examines their relevance to making the data produced FAIR.

This establishes levels of importance of this metadata in the FAIRisation of experimental data and thus determines what priority to give to the metadata that needs to be recorded and which collection tools to focus on first.

This prioritization serves to make a series of recommendations on the types of metadata that should be collected at each stage and these recommendations provide a framework for setting standards on metadata formats to be adopted in common across our research facilities for our different scientific communities.

The document "Final Recommendations for FAIR Photon and Neutron Data Management" complements the first one by suggesting ways of implementing the proposed Metadata Framework in the research facilities. These implementation modalities are discussed and current practices and tools for metadata capture, storage and display are highlighted.

This work also examined the commonalities of the Framework with other initiatives developed within and outside the PaNOSC and ExPaNDS projects, such as a common search API, the data catalogs, the EOSC B2FIND and OpenAire scientific content discovery platforms, and the NeXus data format project. It also provides the data producer with guidelines on the tools and schemes available to record their provenance.

